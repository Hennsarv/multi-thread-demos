﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp30
{
    static class Program
    {
        //        static List<int> numbrid = new List<int>(4_000_000);
        static BlockingCollection<int> numbrid = new BlockingCollection<int>(4000000);

        static int nr = 0;
        static object numbriLukk = new object();
        static void Main(string[] args)
        {

            Thread t1 = new Thread(Tegelane);
            t1.Start(null);
            Thread t2 = new Thread(Tegelane);
            t2.Start(null);
            t1.Join();
            t2.Join();
            Console.WriteLine(numbrid.Count);

            Console.WriteLine(
            numbrid.ToLookup(x => x)
                .Where(x => x.Count() > 1)
                .Count());

        }

        static void Tegelane(object o)
        {
            for (int i = 0; i < 1_000_000; i++)
            {
                int uus;

                lock (numbriLukk)
                {
                    uus = nr;
                    nr = nr + 1;
                }
                numbrid.Add(uus);
            }


        }
    }

}
