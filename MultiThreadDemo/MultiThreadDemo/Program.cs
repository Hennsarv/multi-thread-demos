﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace MultiThreadDemo
{
    static class Program
    {
        static Random rGlobal = new Random();
        static void Main(string[] args)
        {
            var start = DateTime.Now;
            string[] nimed = { "Henn", "Ants", "Peeter" };
            //Console.WriteLine(LongRunning("Henn"));
            //Console.WriteLine(LongRunning("Ants"));
            //Console.WriteLine(LongRunning("Peeter"));

            //nimed.AsParallel().ForAll(
            //    x=>
            //    {
            //        //Console.WriteLine(LongRunning(x));
            //        Console.WriteLine($"{x} alustas");
            //        LongRunning2(x);
            //    }
            //    );

            //Thread t = new Thread(LongRunning4);
            //t.Start("Henn");

            //nimed.Select(x => new { p = x, t = new Thread(LongRunning4) })
            //    .Select(x => { x.t.Start(x.p); return x; })
            //    .ToList().ForEach(x => x.t.Join());


            //var t = nimed.Select(x => LongRunning3(x)).ToArray();
            //Task.WaitAll(t);
            //t.ToList().ForEach(x => Console.WriteLine(x.Result));




            //nimed.Select(x => LongRunning3(x))
            //    .ToList()
            //    .ForEach(x => Console.WriteLine(x.Result));

            


            Console.WriteLine($"kõik kokku: {(DateTime.Now-start).TotalSeconds}");
        
        }

        static string LongRunning(string nimi)
        {
            Console.WriteLine($"{nimi} alustas");
            Random r = new Random(rGlobal.Next());
            var start = DateTime.Now;
            Thread.Sleep(1000 + 1000 * (r.Next() % 20));
            return $"{nimi} kestis {(DateTime.Now - start).TotalSeconds}";
        }

        static void LongRunning2(string nimi)
        {
            Console.WriteLine($"{nimi} alustas");
            Random r = new Random(rGlobal.Next());
            var start = DateTime.Now;
            Thread.Sleep(1000 + 1000 * (r.Next() % 20));
            Console.WriteLine( $"{nimi} kestis {(DateTime.Now - start).TotalSeconds}");
        }

        async static Task<string> LongRunning3(string nimi)
        {
            Console.WriteLine($"{nimi} alustas");
            Random r = new Random(rGlobal.Next());
            var start = DateTime.Now;
            await Task.Delay(1000 + 1000 * (r.Next() % 20));
            return $"{nimi} kestis {(DateTime.Now - start).TotalSeconds}";

        }

        static void LongRunning4(object o)
        {
            if (o is string)
                LongRunning2((string)o);
        }


    }
}
