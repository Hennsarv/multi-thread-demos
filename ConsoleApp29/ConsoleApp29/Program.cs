﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp29
{
    static class Program
    {
        static Random globalR = new Random();

        static void Main(string[] args)
        {
            var start = DateTime.Now;
            //Console.WriteLine(LongRunning("Henn"));
            //Console.WriteLine(LongRunning("Ants"));

            var nimed = new string[] { "Henn", "Ants", "Peeter", "Kalle", "Malle", "Palle", "Ülle", "Pille", "Jaak", "Tiit", "Toomas" };

            var taskud =
            nimed.ToList()
                .Select(x => LongRunning3(x)).ToArray();

            Task.WaitAny(taskud);
            Console.WriteLine($"Näe esimene sai hakkama{taskud.Where(x => x.IsCompleted).SingleOrDefault()?.Result}");

            taskud.Where(x => !x.IsCompleted).ToList()
                .ForEach(x => x.Dispose());



            Console.WriteLine($"kokku kulus aega {(DateTime.Now - start).TotalSeconds}");
        }

        static string LongRunning(string nimi)
        {
            Console.WriteLine($"{nimi} alustas");
            Random r = new Random(globalR.Next());

            var start = DateTime.Now;

            Thread.Sleep(1000 + 1000 * (r.Next() % 10));

            return $"{nimi} kestis {(DateTime.Now - start).TotalSeconds}";
        }
        async static Task<string> LongRunning3(string nimi)
        {
            Console.WriteLine($"{nimi} alustas");
            Random r = new Random(globalR.Next());

            var start = DateTime.Now;

            await Task.Delay(1000 + 1000 * (r.Next() % 10));

            return $"{nimi} kestis {(DateTime.Now - start).TotalSeconds}";
        }



    }

    
}
