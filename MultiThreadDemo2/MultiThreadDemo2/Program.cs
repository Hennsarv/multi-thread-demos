﻿using System;
using System.Collections.Generic;
using conc = System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MultiThreadDemo2
{
    static class Program
    {
        static List<int> numbrid = new List<int>(2000000);
        //static conc.BlockingCollection<int> numbrid = new conc.BlockingCollection<int>(4000000);
        static int nr = 0;
        static object lukk = new object();
        static Random r = new Random();
        static void Main(string[] args)
        {
            Thread t1 = new Thread(Tegelane);
            t1.Start(" Esimene  ");
            Thread t2 = new Thread(Tegelane);
            t2.Start(" Teine    ");
            t1.Join();
            t2.Join();
            Console.WriteLine(numbrid.Count);
            Console.WriteLine(numbrid.Distinct().Count());
            numbrid.ToLookup(x => x).Where(x => x.Count() > 1).ToList()
                .ForEach(x => Console.WriteLine($"{x.Key} - {x.Count()}"));

        }

        static void Tegelane(object o)
        {
            for (int i = 0; i < 1000000 ; i++)
            {
                //System.Threading.Thread.Sleep(1000 * (r.Next() % 2));
                //Console.Write($"{o} - {nr = nr + 1:0000000}");

                int uus;

                //lock (lukk)
                {
                    uus = nr++;

                }

                numbrid.Add(uus);


            }
        }
    }
}
